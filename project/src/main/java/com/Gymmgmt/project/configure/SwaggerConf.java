package com.Gymmgmt.project.configure;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.Collections;

@Configuration
public class SwaggerConf {
    @Bean
    public Docket Swaggerconfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
//                .paths(PathSelectors.ant("/api/v1/gymManagement/*"))
                .apis(RequestHandlerSelectors.basePackage("com.Gymmgmt.project"))
                .build()
                .apiInfo(metadata());
    }

    private ApiInfo metadata() {
        return new ApiInfo("Gym management Api",
                "Gym management project",
                "1.0",
                "",
                new Contact("", "", ""),
                "Apache 2.0",
                "",
                new ArrayList<>());


    }
}
