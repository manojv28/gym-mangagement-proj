package com.Gymmgmt.project.dataaccesss.model;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Document(collection = "persons")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Component
public class Customer extends Person {
    @Transient
    public static final String SEQUENCE_NAME = "C";
    private String payfees;
}
