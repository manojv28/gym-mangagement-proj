package com.Gymmgmt.project.dataaccesss.repository.extdImp;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Trainer;
import com.Gymmgmt.project.dataaccesss.repository.extended.TrainerRepositoryExtended;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public class TrainerRepositoryImpl implements TrainerRepositoryExtended {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public List<Trainer> findtrainer() {
        Query query = new Query();
        Person person =new Person();
        query.addCriteria(where("trainerid").exists(false));
        return mongoTemplate.find(query, Trainer.class);
    }

    @Override
    public UpdateResult updateSalary(String id, String salary) {
        Query query=new Query();

        query.addCriteria(where("id").is(id));
        Update update=new Update().set("salary",salary);
        return mongoTemplate.upsert(query,update,Person.class);
    }

    @Override
    public UpdateResult updateExperiences(String id, String experiance) {
        Query query=new Query();

        query.addCriteria(where("id").is(id));
        Update update=new Update().set("experiance",experiance);
        return mongoTemplate.upsert(query,update,Person.class);
    }
}
