package com.Gymmgmt.project.dataaccesss.repository.extdImp;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Customer;
import com.Gymmgmt.project.dataaccesss.repository.extended.CustomerRepositoryExtended;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public class CustomerRepositoryImpl implements CustomerRepositoryExtended {
    @Autowired
    private MongoTemplate mongoTemplate;
    @Override
    public List<Customer> findOnlyCustomers() {
        Query query = new Query();
        Person person = new Person();
        query.addCriteria(where("trainerid").exists(true));
        return mongoTemplate.find(query, Customer.class);
    }

    @Override
    public UpdateResult updatepayfees(String id, String payfee) {
        Query query=new Query();

        query.addCriteria(where("id").is(id));
        Update update=new Update().set("payfees",payfee);
        return mongoTemplate.upsert(query,update,Person.class);
    }
}
