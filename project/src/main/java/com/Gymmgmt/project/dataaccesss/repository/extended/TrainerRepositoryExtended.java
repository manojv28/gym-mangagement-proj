package com.Gymmgmt.project.dataaccesss.repository.extended;

import com.Gymmgmt.project.dataaccesss.model.Trainer;
import com.mongodb.client.result.UpdateResult;

import java.util.List;

public interface TrainerRepositoryExtended {
    List<Trainer> findtrainer();
    UpdateResult updateSalary(String id, String salary);
    UpdateResult updateExperiences(String id, String experiance);
}
