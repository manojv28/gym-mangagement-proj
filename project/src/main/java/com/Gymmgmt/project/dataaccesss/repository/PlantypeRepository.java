package com.Gymmgmt.project.dataaccesss.repository;

import com.Gymmgmt.project.dataaccesss.entity.Plantype;
import com.Gymmgmt.project.dataaccesss.repository.extended.PlantypeRepositoryExtended;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantypeRepository extends MongoRepository <Plantype,String>, PlantypeRepositoryExtended {
}
