package com.Gymmgmt.project.dataaccesss.repository.extended;

import com.Gymmgmt.project.dataaccesss.entity.Equipment;

import java.util.List;

public interface EqipmentRepositoryExtended {
    List<Equipment> findEquipmentbyId(String id);
    Equipment updateWorkoutid(String id, String wid);
    Equipment removeWorkout(String id, String wid);
}
