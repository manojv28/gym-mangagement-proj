package com.Gymmgmt.project.dataaccesss.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "plantypes")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Plantype {
    @Id
    private String id;
    private String name;
    private String day1;
    private String day2;
    private String day3;
    private String day4;
}
