package com.Gymmgmt.project.dataaccesss.repository;


import com.Gymmgmt.project.dataaccesss.entity.Equipment;
import com.Gymmgmt.project.dataaccesss.repository.extended.EqipmentRepositoryExtended;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentRepository extends MongoRepository<Equipment, String>, EqipmentRepositoryExtended {
}
