package com.Gymmgmt.project.dataaccesss.repository;

import com.Gymmgmt.project.dataaccesss.entity.WorkoutPlan;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkoutRepository extends MongoRepository<WorkoutPlan, String> {
}
