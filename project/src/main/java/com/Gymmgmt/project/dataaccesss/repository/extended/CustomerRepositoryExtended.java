package com.Gymmgmt.project.dataaccesss.repository.extended;

import com.Gymmgmt.project.dataaccesss.model.Customer;
import com.mongodb.client.result.UpdateResult;

import java.util.List;

public interface CustomerRepositoryExtended {
    List<Customer> findOnlyCustomers();
    UpdateResult updatepayfees(String id, String payfee);
}
