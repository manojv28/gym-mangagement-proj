package com.Gymmgmt.project.dataaccesss.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "equipments")

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Equipment {

    @Id
    private String eid;
    private String name;
    private String desc;
    private String price;
    private List<String> workoutid;
    private Boolean repair;
}
