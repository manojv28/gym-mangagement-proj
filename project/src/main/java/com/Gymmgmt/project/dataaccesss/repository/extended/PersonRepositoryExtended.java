package com.Gymmgmt.project.dataaccesss.repository.extended;

import com.Gymmgmt.project.dataaccesss.entity.Person;

import java.util.List;

public interface PersonRepositoryExtended {
    Person findOneByName(String name);

    List<Person> findByName(String name);

    List<Person> findCustomerbyId(String id);

    public String getSequence(String seq);

    public List<Person> findCustomersBywid(String id);

}
