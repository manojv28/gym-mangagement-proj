package com.Gymmgmt.project.dataaccesss.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "workoutplans")
@AllArgsConstructor
@Data
public class WorkoutPlan {
    @Id
    private String wid;
    private String name;
    private Integer price;
    private String duration;
    @DBRef
    private List<Plantype> plantype;
}
