package com.Gymmgmt.project.dataaccesss.entity;

import com.Gymmgmt.project.dataaccesss.model.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

@Document(collection = "persons")

@NoArgsConstructor
@AllArgsConstructor
@Data
@Component
public class Person {

    @Id
    private String id;
    private String name;
    private Integer age;
    private Address address;
    private String phno;
    private String gender;
    private String trainerid;
    private String workoutid;
}
