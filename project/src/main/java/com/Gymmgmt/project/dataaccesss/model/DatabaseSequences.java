package com.Gymmgmt.project.dataaccesss.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sequences")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DatabaseSequences {

    @Id
    private String id;
    private long seq;
}

