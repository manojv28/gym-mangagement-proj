package com.Gymmgmt.project.dataaccesss.repository.extdImp;

import com.Gymmgmt.project.dataaccesss.entity.Equipment;
import com.Gymmgmt.project.dataaccesss.repository.extended.EqipmentRepositoryExtended;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public class EquipmentRepositoryImpl implements EqipmentRepositoryExtended {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Equipment> findEquipmentbyId(String id) {
        Query query = new Query();
        query.addCriteria(where("workoutid").is(id));
        return mongoTemplate.find(query, Equipment.class);
    }

    public Equipment updateWorkoutid(String id, String wid) {
        Query query = new Query();
        Equipment equipment = new Equipment();
        query.addCriteria(where("eid").is(id));
        Update update = new Update().push("workoutid", wid);
        return mongoTemplate.findAndModify(query, update, Equipment.class);
    }

    @Override
    public Equipment removeWorkout(String id, String wid) {
        Query query = new Query();
        Equipment equipment = new Equipment();
        query.addCriteria(where("eid").is(id));
        Update update = new Update().pull("workoutid", wid);
        return mongoTemplate.findAndModify(query, update, Equipment.class);
    }
}
