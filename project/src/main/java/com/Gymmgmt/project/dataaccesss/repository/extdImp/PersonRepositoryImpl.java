package com.Gymmgmt.project.dataaccesss.repository.extdImp;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.DatabaseSequences;
import com.Gymmgmt.project.dataaccesss.repository.extended.PersonRepositoryExtended;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

public class PersonRepositoryImpl implements PersonRepositoryExtended {
    @Autowired
    private MongoTemplate mongoTemplate;


    public Person findOneByName(String name) {
        return null;
    }


    public List<Person> findByName(String name) {
        Query query = new Query();
        query.addCriteria(where("name").is(name));
        return mongoTemplate.find(query, Person.class);
    }

    //find all customers under trainer
    @Override
    public List<Person> findCustomerbyId(String id) {
        Query query = new Query();
        query.addCriteria(where("trainerid").is(id));
        return mongoTemplate.find(query, Person.class);
    }

    //find customers under that workoutplan
    @Override
    public List<Person> findCustomersBywid(String id) {
        Query query = new Query();
        query.addCriteria(where("workoutid").is(id));
        return mongoTemplate.find(query, Person.class);
    }

    //find all customers

    //find all trainer


    //id auto generation
    @Override
    public String getSequence(String seq) {
        DatabaseSequences counter = mongoTemplate.findAndModify(query(where("id").is(seq)),
                new Update().inc("seq", 1), options().returnNew(true).upsert(true),
                DatabaseSequences.class);
        return seq.concat(String.valueOf(!Objects.isNull(counter) ? counter.getSeq() : 1));
    }


    public Person findOneById(String id) {
        return null;
    }


}
