package com.Gymmgmt.project.dataaccesss.repository;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.repository.extended.TrainerRepositoryExtended;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerRepository extends MongoRepository<Person,String>, TrainerRepositoryExtended {
}
