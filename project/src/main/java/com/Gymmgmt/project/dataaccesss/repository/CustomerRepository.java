package com.Gymmgmt.project.dataaccesss.repository;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.repository.extended.CustomerRepositoryExtended;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public interface CustomerRepository extends MongoRepository<Person,String> , CustomerRepositoryExtended {
}
