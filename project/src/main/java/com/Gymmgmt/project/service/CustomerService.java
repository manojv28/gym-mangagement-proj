package com.Gymmgmt.project.service;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Customer;
import com.Gymmgmt.project.dataaccesss.repository.CustomerRepository;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAllCustomer() {
        return customerRepository.findOnlyCustomers();
    }
    public UpdateResult updatepayfees(String id,String payfees){
        return customerRepository.updatepayfees(id,payfees);
    }
}
