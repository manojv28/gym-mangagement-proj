package com.Gymmgmt.project.service;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Trainer;
import com.Gymmgmt.project.dataaccesss.repository.PersonRepository;
import com.Gymmgmt.project.dataaccesss.repository.TrainerRepository;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TrainerService {

    @Autowired
    private TrainerRepository trainerRepository;
    @Autowired
    private PersonRepository personRepository;

    public List<Trainer> findtrainer() {
        return trainerRepository.findtrainer();
    }

    public void insertTrainer(Person person) {
        person.setId(personRepository.getSequence(Trainer.SEQUENCE_NAME));
        this.personRepository.insert(person);
    }
    public UpdateResult updateSalary(String id, String salary){
        return trainerRepository.updateSalary(id,salary);
    }
    public UpdateResult updateexperience(String id,String expetience){
        return trainerRepository.updateExperiences(id,expetience);
    }

}
