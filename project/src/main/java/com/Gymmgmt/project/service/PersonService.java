package com.Gymmgmt.project.service;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Customer;
import com.Gymmgmt.project.dataaccesss.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> getAll() {
        return personRepository.findAll();
    }

    public void insertAll(Person person) {
        person.setId(personRepository.getSequence(Customer.SEQUENCE_NAME));
        this.personRepository.insert(person);
    }



    public void deleteAll() {
        this.personRepository.deleteAll();
    }

    public void deletebyid(String id) {
        this.personRepository.deleteById(id);
    }

    public List<Person> findbyname(String name) {
        return personRepository.findByName(name);
    }

    public void update(Person person) {
        this.personRepository.save(person);
    }

    public List<Person> findcustomers(String id) {
        return personRepository.findCustomerbyId(id);
    }

    public List<Person> findcustomerBywid(String id) {
        return personRepository.findCustomersBywid(id);
    }
}
