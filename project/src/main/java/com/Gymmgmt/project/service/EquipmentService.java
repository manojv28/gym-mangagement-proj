package com.Gymmgmt.project.service;

import com.Gymmgmt.project.dataaccesss.entity.Equipment;
import com.Gymmgmt.project.dataaccesss.repository.EquipmentRepository;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentService {
    @Autowired
    private EquipmentRepository equipmentRepository;

    public List<Equipment> getAll() {
        return equipmentRepository.findAll();
    }

    public void insertAll(Equipment equipment) {
        this.equipmentRepository.insert(equipment);
    }

    public void deletebyid(String id) {
        this.equipmentRepository.deleteById(id);
    }

    public void update(Equipment equipment) {
        this.equipmentRepository.save(equipment);
    }

    public List<Equipment> findEquiment(String id) {
        return equipmentRepository.findEquipmentbyId(id);
    }

    public Equipment addworkuotid(String id, String wid) {
        return equipmentRepository.updateWorkoutid(id, wid);
    }

    public Equipment removeWorkoutid(String id, String wid) {
        return equipmentRepository.removeWorkout(id, wid);
    }


}
