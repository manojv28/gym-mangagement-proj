package com.Gymmgmt.project.service;

import com.Gymmgmt.project.dataaccesss.entity.Plantype;
import com.Gymmgmt.project.dataaccesss.repository.PlantypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanTypeService {
    @Autowired
    private PlantypeRepository plantypeRepository;

    public List<Plantype> getAll() {
        return plantypeRepository.findAll();
    }

    public void insertAll(Plantype plantype) {
        this.plantypeRepository.insert(plantype);
    }

    public void deletebyid(String id) {
        this.plantypeRepository.deleteById(id);
    }

    public void update(Plantype plantype) {
        this.plantypeRepository.save(plantype);
    }

}
