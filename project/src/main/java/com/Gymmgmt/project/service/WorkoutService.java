package com.Gymmgmt.project.service;

import com.Gymmgmt.project.dataaccesss.entity.WorkoutPlan;
import com.Gymmgmt.project.dataaccesss.repository.WorkoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkoutService {
    @Autowired
    private WorkoutRepository workoutRepository;

    public List<WorkoutPlan> getAll() {
        return workoutRepository.findAll();
    }

    public void insertAll(WorkoutPlan WorkoutPlan) {
        this.workoutRepository.insert(WorkoutPlan);
    }

    public void deletebyid(String id) {
        this.workoutRepository.deleteById(id);
    }

    public void update(WorkoutPlan workoutPlan) {
        this.workoutRepository.save(workoutPlan);
    }
}
