package com.Gymmgmt.project.controller;

import com.Gymmgmt.project.dataaccesss.model.Customer;
import com.Gymmgmt.project.service.CustomerService;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/gymManagement/customer")
public class CustomerController {
    @Autowired

    private CustomerService customerServicel;

    @GetMapping("/customer")
    public List<Customer> getCustomer() {
        return customerServicel.findAllCustomer();
    }

    @PutMapping("/{id}/{payfees}")
    public UpdateResult getfees(@PathVariable("id") String id, @PathVariable("payfees") String payfees) {
        return customerServicel.updatepayfees(id, payfees);
    }

}
