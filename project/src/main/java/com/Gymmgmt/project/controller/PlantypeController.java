package com.Gymmgmt.project.controller;

import com.Gymmgmt.project.dataaccesss.entity.Plantype;
import com.Gymmgmt.project.service.PlanTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/gymManagement/plantype")
public class PlantypeController {

    @Autowired
    private PlanTypeService planTypeService;

    @GetMapping
    public List<Plantype> getPlantype() {
        return planTypeService.getAll();
    }

    @PostMapping
    public void insertPlantype(@RequestBody Plantype plantype) {
        this.planTypeService.insertAll(plantype);
    }

    @DeleteMapping("/{id}")
    public void deleteByid(@PathVariable("id") String id) {
        this.planTypeService.deletebyid(id);
    }

    @PutMapping
    public void update(@RequestBody Plantype plantype) {
        this.planTypeService.update(plantype);
    }

}
