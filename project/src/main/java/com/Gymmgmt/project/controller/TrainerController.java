package com.Gymmgmt.project.controller;


import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Trainer;
import com.Gymmgmt.project.service.TrainerService;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/gymManagement/trainer")
public class TrainerController {
    @Autowired
    private TrainerService trainerService;

    @GetMapping("/trainer")
    public List<Trainer> gettrainer() {
        return trainerService.findtrainer();
    }

    @PostMapping("/insert")
    public void insertTrainer(@RequestBody Trainer trainer) {
        this.trainerService.insertTrainer(trainer);
    }

    @PutMapping("/{id}/{salary}")
    public UpdateResult getfees(@PathVariable("id") String id, @PathVariable("salary") String salary) {
        return trainerService.updateSalary(id, salary);
    }
    @PutMapping("/updateExperience/{id}/{experince}")
    public UpdateResult updateexperience(@PathVariable("id") String id, @PathVariable("experince") String experince) {
        return trainerService.updateexperience(id, experince);
    }
}
