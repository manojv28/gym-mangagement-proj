package com.Gymmgmt.project.controller;

import com.Gymmgmt.project.dataaccesss.entity.Person;
import com.Gymmgmt.project.dataaccesss.model.Customer;
import com.Gymmgmt.project.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@EnableSwagger2
@RestController
@RequestMapping("/api/v1/gymManagement/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    @GetMapping
    public List<Person> getall() {
        return personService.getAll();
    }


    @PostMapping("/save")
    public void insertCustomer(@RequestBody Customer customer) {
        this.personService.insertAll(customer);
    }


    @DeleteMapping("/{id}")
    public void deleteByid(@PathVariable("id") String id) {
        this.personService.deletebyid(id);
    }

    @GetMapping("/name/{name}")
    public List<Person> getByname(@PathVariable("name") String name) {
        return personService.findbyname(name);
    }

    @PutMapping
    public void update(@RequestBody Person person) {
        this.personService.update(person);
    }

    @GetMapping("/trainerid/{trainerid}")
    public List<Person> getcusbytid(@PathVariable("trainerid") String id) {
        return personService.findcustomers(id);
    }

    @GetMapping("/Workid/{Workid}")
    public List<Person> getcusByWid(@PathVariable("Workid") String id) {
        return personService.findcustomerBywid(id);
    }
}
