package com.Gymmgmt.project.controller;

import com.Gymmgmt.project.dataaccesss.entity.WorkoutPlan;
import com.Gymmgmt.project.service.PlanTypeService;
import com.Gymmgmt.project.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/gymManagement/workoutplan")
public class WorkoutController {

    @Autowired
    private WorkoutService workoutService;

    @GetMapping
    public List<WorkoutPlan> getWorkoutplan() {
        return workoutService.getAll();
    }

    @PostMapping
    public void insertWorkoutplan(@RequestBody WorkoutPlan workoutPlan) {
        this.workoutService.insertAll(workoutPlan);
    }

    @DeleteMapping("/{id}")
    public void deleteByid(@PathVariable("id") String id) {
        this.workoutService.deletebyid(id);
    }

    @PutMapping
    public void update(@RequestBody WorkoutPlan workoutPlan) {
        this.workoutService.update(workoutPlan);
    }
}
