package com.Gymmgmt.project.controller;

import com.Gymmgmt.project.dataaccesss.entity.Equipment;
import com.Gymmgmt.project.service.EquipmentService;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/gymManagement/equipment")
public class EquipmentController {
    @Autowired
    private EquipmentService equipmentService;

    @GetMapping
    public List<Equipment> getEquipment() {
        return equipmentService.getAll();
    }

    @PostMapping
    public void insertEqipment(@RequestBody Equipment equipment) {
        this.equipmentService.insertAll(equipment);
    }

    @DeleteMapping("/{id}")
    public void deleteByid(@PathVariable("id") String id) {
        this.equipmentService.deletebyid(id);
    }

    @PutMapping
    public void update(@RequestBody Equipment equipment) {
        this.equipmentService.update(equipment);
    }

    @GetMapping("/work/{workid}")
    public List<Equipment> getEqipmentid(@PathVariable("workid") String id) {
        return equipmentService.findEquiment(id);
    }
    @PutMapping("/{id}/{wid}")
    public Equipment addworkuotid(@PathVariable("id") String id,@PathVariable("wid") String wid){
        return equipmentService.addworkuotid(id,wid);
    }
    @PutMapping("/remove/{id}/{wid}")
    public Equipment removeworkuotid(@PathVariable("id") String id,@PathVariable("wid") String wid){
        return equipmentService.removeWorkoutid(id,wid);
    }
}
